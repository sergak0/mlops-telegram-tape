# -*- coding: utf-8 -*-
import click
import logging
import pandas as pd
import bs4


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath: str, output_filepath: str):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """

    logger = logging.getLogger(__name__)
    logger.info('Making final data set from raw data')

    chat_text = open(input_filepath, 'r').read()
    soup = bs4.BeautifulSoup(chat_text, features="html.parser")

    res = []
    for message in soup.find_all('div', {"class": "message default clearfix"}):
        now = {'text': '',
               'photos': [],
               'from_name': None}

        text = message.find('div', {'class': 'text'})
        if text is not None:
            now['text'] = text.text

        media = message.find('div', {'class': 'media_wrap clearfix'})
        if media is not None:
            now["photos"] = [el['href'] for el in media.find_all('a')]

        from_name = message.find('div', {'class': 'forwarded body'})
        if from_name is not None:
            now['from_name'] = from_name.find('div', {'class': 'from_name'}).text

        res.append(now)

    df_res = pd.DataFrame(res)
    df_res.to_csv(output_filepath, index=False)
    logger.info('Chat has been processed, it contains {} messages'.format(len(df_res)))


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # python3 src/data/make_dataset.py data/raw/Dl_notes/messages.html data/interim/Dl_notes.csv
    main()

from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Telegram bot for creating personal content recommendation',
    author='Sergey Volnov',
    license='MIT',
)
